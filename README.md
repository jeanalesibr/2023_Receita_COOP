# 2023_Receita_COOP
* Fonte: https://dados.gov.br/dados/conjuntos-dados/cadastro-nacional-da-pessoa-jurdica---cnpj

* Descrição das tabelas e relacionamentos no arquivo nomeado como dicionario.pdf

 
  
* 1 - Baixe todas os arquivos COMPACTADOS das tabelas presentes no dicionario.pdf, salve os arquivos compactados dentro da pasta: dados_entrada

* 2 - Execute todos os scripts na ORDEM.  

* 3 - O resultado irá aparecer na pasta dados_finais.




*OBS: exclua se quiser depois os arquivos de dados_entrada e dados_processamento
